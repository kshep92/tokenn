# Tokenn

> "Sigh... how do you use the {insert provider here} JS API again?..."

**Tokenn** is a library for retrieving access tokens from identity providers. Forget spending hours
poring over documentation and tedious trial and error. Use Tokenn and get crackin'!

Currently supported:

- Facebook
- Google

## Delightfully simple API

```
//Facebook login
var facebook = new FacebookAuth('{my-app-id}', ['user_profile', 'email', 'birthday']);

facebook.login()
    .then((data) => {
        /* { accessToken: 'token', userId: 12345 } */
    }).catch((response) => { /*Handle the error*/ })

//Google login
var google = new GoogleAuth('{my-client-id}', ['profile', 'email']);

google.login()
    .then((data) => { /* {accessToken: 'token', idToken: '123' } */});

```

## Usage

### Include Tokenn library

The Tokenn library can be included in one of two ways:

1. As a single file
2. On a per-provider basis.

**Single file usage**

It is recommended that you use the whole Tokenn library for the sake of simplicity. Included in one file is:
- PromiseJS (Polyfill for native ES6/7 promises)
- Facebook JavaScript SDK
- Google JavaScript SDK
- Tokenn API wrappers

All of that at around 70K gzipped!

Include the Tokenn library on your page as follows:
```
<script src="tokenn/tokenn.js"></script>
```

**Per-provider usage**

If you prefer a modular approach, you can use only the parts of the Tokenn library you need.
```
<script src="tokenn/lib/promise-js.js"></script> <!-- Promise library -->
<script src="tokenn/facebook-auth.js"></script> <!-- For Facebook login -->
<script src="tokenn/google-auth.js"></script> <!-- For Google login -->
```

## Examples

### Facebook login

The following code will trigger the Facebook Login dialog when the button is clicked.

```
<button id="btnFacebook">Facebook Login</button>
<script src="tokenn/tokenn.js"></script>
<script>
    var facebook = new FacebookAuth('my-app-id');
    document.getElementById("btnFacebook").addEventListener("click", function() {
                facebook.login()
                    .then(function(response) {
                        console.log(response)
                     }).catch(function(response) {
                        console.log(response);
                     });
            });
</script>

```

When the `FacebookAuth#login` method is called, it returns a promise. Upon successful login, the data returned will be a
JSON object containing the following fields:

* `accessToken`: The Facebook access token that you will send to your server to get the rest of the user details.
* `userId`: The ID of the user on Facebook.

To understand how to use this data, see the [Facebook developer documentation](https://developers.facebook.com/docs/facebook-login/access-tokens)
or the [Facebook Graph API documentation](https://developers.facebook.com/docs/graph-api/reference/user).

### Google login

The following code will trigger the Google Sign-In dialog when the button is clicked.

```
<button id="btnGoogle">Google Login</button>
<script src="tokenn/tokenn.js"></script>
<script>
    var google = new GoogleAuth("my-client-id");
    document.getElementById("btnGoogle").addEventListener("click", function() {
                google.login().then(function(response) {
                    console.log(response);
                });
            });
</script>
```

When the `GoogleAuth#login` method is called, it returns a promise. Upon successful login, the data returned will be a
JSON object containing the following fields:

* `accessToken`: The Google access token that you will send to your server to get the rest of the user details
* `idToken`: The ID token needed to get the rest of the user via the Google API.

To understand how to use this data, see the [Google Sign-In developer documentation](https://developers.google.com/identity/sign-in/web/backend-auth)
or check out [this YouTube video](https://www.youtube.com/watch?v=j_31hJtWjlw).

## API

### FacebookAuth

```
new FacebookAuth("[app-id]", "[api-version]")
```

* `app-id`: The App ID created for you on the [Facebook developer console](https://developers.facebook.com/). **Required**
* `api-verson`: The version of the Facebook API you wish to use. _Optional_. Default: `"v2.5"`

```
FacebookAuth#login([permissions])
```

* `permissions`: An array of permissions your login requires. See [reference](https://developers.facebook.com/docs/facebook-login/permissions). _Optional_
Default: `['public_profile', 'email']`

### GoogleAuth

```
new GoogleAuth("[client-id]", [permissions])
```

* `client-id`: The Client ID generated for your app on the [Google developers console](https://developers.google.com/identity/sign-in/web/devconsole-project). **Required**
* `permissions`: An array of permissions your login requires. See [reference](https://developers.google.com/identity/protocols/googlescopes#google_sign-in).
 _Optional._ Default: `['profile', 'email']`
