var GoogleAuth = (function () {
    function GoogleAuth(_clientId, permissions) {
        var _this = this;
        if (permissions === void 0) { permissions = ['profile', 'email']; }
        this._clientId = _clientId;
        this._scope = permissions.join(' ');
        console.log(this._clientId);
        gapi.load('auth2', function () {
            gapi.auth2.init({
                client_id: _this._clientId,
                scope: _this._scope
            }).then(function () {
                _this.auth2 = gapi.auth2.getAuthInstance();
                console.log("GAPI Initialized.");
            });
        });
    }
    GoogleAuth.prototype.login = function () {
        var _this = this;
        if (this.auth2) {
            return new Promise(function (resolve) {
                _this.auth2.signIn().then(function (response) {
                    var authResponse = response.getAuthResponse();
                    resolve({ accessToken: authResponse.access_token, idToken: authResponse.id_token });
                });
            });
        }
        else {
            console.error("Google Auth API not initialized yet.");
        }
    };
    return GoogleAuth;
})();
